namespace Rbg
{
    public class CharacterEffect
    {
        public CharacterEffectType Type;

        public double Value;

        CharacterEffect(CharacterEffectType type, double value)
        {
            Type = type;
            Value = value;
        }

        public double Apply(double damageTaken)
        {
            var newDamageTaken = damageTaken;

            switch (Type)
            {
                case CharacterEffectType.Heal:
                    newDamageTaken -= Value;
                    break;
                case CharacterEffectType.Damage:
                    newDamageTaken += Value;
                    break;
            }

            return Utils.Clamp(newDamageTaken, 0, double.MaxValue);
        }
    }
}
