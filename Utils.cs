namespace Rbg
{
    public class Utils
    {
        public static double Clamp(double currentValue, double minValue, double maxValue)
        {
            if (currentValue < minValue)
            {
                return minValue;
            }

            if (currentValue > maxValue)
            {
                return maxValue;
            }

            return currentValue;
        }
    }
}
