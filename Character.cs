namespace Rbg
{
    public class Character
    {
        public string Name;

        public double BaseHealthPoints;

        public double BasePhysicalAttack;

        public double BaseMagicalAttack;

        public double BaseArmor;

        public double BaseSpeed;

        public double BasePhysicalDamageResistance;

        public double BaseMagicalDamageResistance;

        public CharacterAbility[] Abilities;

        public Buff[] Buffs;

        public double DamageTaken;

        public CharacterEffect[] CharacterEffects;

        public Character(string name, double healthPoints, double physicalAttack, double magicalAttack, double armor, double speed, double physicalDamageResistance, double magicalDamageResistance, CharacterAbility[] abilities, Buff[] buffs, CharacterEffect[] characterEffects)
        {
            Name = name;
            BaseHealthPoints = healthPoints;
            BasePhysicalAttack = physicalAttack;
            BaseMagicalAttack = magicalAttack;
            BaseArmor = armor;
            BaseSpeed = speed;
            BasePhysicalDamageResistance = physicalDamageResistance;
            BaseMagicalDamageResistance = magicalDamageResistance;
            Abilities = abilities;
            Buffs = buffs;
            DamageTaken = 0;
            CharacterEffects = characterEffects;
        }

        public double HealthPoints => GetBuffedValue(BaseHealthPoints, BuffType.HealthPoints) - DamageTaken;
        public double PhysicalAttackDamage => GetBuffedValue(BasePhysicalAttack, BuffType.PhysicalAttack);
        public double MagicalAttackDamage => GetBuffedValue(BaseMagicalAttack, BuffType.MagicalAttack);
        public double Armor => GetBuffedValue(BaseArmor, BuffType.Armor);
        public double Speed => GetBuffedValue(BaseSpeed, BuffType.Speed);
        public double PhysicalDamageResistance => GetBuffedValue(BasePhysicalDamageResistance, BuffType.PhysicalDamageResistance);
        public double MagicalDamageResistance => GetBuffedValue(BaseMagicalDamageResistance, BuffType.MagicalDamageResistance);

        public bool IsAlive => HealthPoints - DamageTaken > 0;

        public void UpdateBuffsForNextRound()
        {
            var newBuffs = new List<Buff>();

            foreach (var buff in Buffs)
            {
                buff.DecrementRoundsLeft();

                if (!buff.ShouldUseInNextRound)
                    continue;

                newBuffs.Add(buff);
            }

            Buffs = newBuffs.ToArray();
        }

        /// <summary>
        /// Removes all round based buffs.
        /// Should be called after a battle has ended so only permanent buffs will be contained in the list of active buffs.
        /// </summary>
        public void RemoveRoundBasedBuffs()
        {
            Buffs = Buffs.Where(buff => buff.IsPermanent).ToArray();
        }

        private double GetBuffedValue(double baseValue, BuffType buffType)
        {
            return Buffs.Where(buff => buff.Type == buffType).Aggregate(baseValue, (intermediateValue, buff) => buff.Apply(intermediateValue));
        }

        public void ApplyEffects()
        {
            DamageTaken = CharacterEffects.Aggregate(DamageTaken, (intermediateValue, effect) => effect.Apply(intermediateValue));
        }

        /// <summary>
        /// Clones the current character.
        /// This should be used if the characters (like allies / enemies) are defined statically.
        /// </summary>
        /// <returns>The cloned character</returns>
        public Character Clone()
        {
            var clonedBuffs = Buffs.Select(buff => buff.Clone()).ToArray();

            return new Character(
                Name,
                BaseHealthPoints,
                BasePhysicalAttack,
                BaseMagicalAttack,
                BaseArmor,
                BaseSpeed,
                BasePhysicalDamageResistance,
                BaseMagicalDamageResistance,
                Abilities,
                clonedBuffs,
                []
            );
        }
    }
}
