﻿using Rbg;
using Rbg.Abilities;

Console.Clear();

var character = new Character(
    name: "Some Mage",
    healthPoints: 10,
    physicalAttack: 1,
    magicalAttack: 1,
    armor: 0,
    speed: 1,
    physicalDamageResistance: 1,
    magicalDamageResistance: 1,
    abilities: [
        new Tackle()
    ],
    buffs: [
        new Buff(type: BuffType.HealthPoints, value: 2, roundsLeft: 1, buffApplicationType: BuffApplicationType.Multiplicative),
    ],
    characterEffects: []
);

Console.WriteLine("Health points with buffs: {0}", character.HealthPoints);
character.UpdateBuffsForNextRound();
Console.WriteLine("Health points without buffs: {0}", character.HealthPoints);
