namespace Rbg
{
    public abstract class CharacterAbility
    {
        public string Name;

        public DamageType DamageType;

        public CharacterAbility(string name, DamageType damageType)
        {
            Name = name;
            DamageType = damageType;
        }

        public abstract void Use(PlacedCharacter source, Vector2 position, PlacedCharacter[] affectedCharacters);
    }
}
