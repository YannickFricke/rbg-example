namespace Rbg
{
    public class Battlefield
    {
        public List<PlacedCharacter> Allies;

        public List<PlacedCharacter> Enemies;

        public Battlefield(List<PlacedCharacter> allies, List<PlacedCharacter> enemies)
        {
            Allies = allies;
            Enemies = enemies;
        }
    }
}
