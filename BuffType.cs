namespace Rbg
{
    public enum BuffType
    {
        PhysicalDamageResistance,

        MagicalDamageResistance,

        HealthPoints,

        Speed,

        Armor,

        PhysicalAttack,

        MagicalAttack,
    }
}
