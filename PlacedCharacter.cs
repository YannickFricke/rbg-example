namespace Rbg
{
    public class PlacedCharacter
    {
        public Character Character;

        public Vector2 Position;

        public PlacedCharacter(Character character, Vector2 position)
        {
            Character = character;
            Position = position;
        }
    }
}
