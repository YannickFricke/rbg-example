namespace Rbg
{
    public class Buff
    {
        public BuffType Type;

        public double Value;

        public int RoundsLeft;

        public BuffApplicationType BuffApplicationType;

        public Buff(BuffType type, double value, int roundsLeft, BuffApplicationType buffApplicationType)
        {
            if (value == 0 && buffApplicationType == BuffApplicationType.Multiplicative)
            {
                throw new ArgumentException("value can't be 0 for multiplicative operations", nameof(value));
            }

            Type = type;
            Value = value;
            RoundsLeft = roundsLeft;
            BuffApplicationType = buffApplicationType;
        }

        public bool IsPermanent => RoundsLeft == -1;

        public void DecrementRoundsLeft()
        {
            if (IsPermanent)
            {
                return;
            }

            RoundsLeft -= 1;
        }

        public bool ShouldUseInNextRound => IsPermanent || RoundsLeft > 0;

        public double Apply(double baseValue)
        {
            var newValue = baseValue;

            switch (BuffApplicationType)
            {
                case BuffApplicationType.Additive:
                    newValue += Value;
                    break;

                case BuffApplicationType.Multiplicative:
                    newValue *= Value;
                    break;
            }

            return newValue;
        }

        public override string ToString()
        {
            if (IsPermanent)
            {
                return $"PermanentBuff(Type: {Type}, value: {Value}, BuffApplicationType: {BuffApplicationType})";
            }

            return $"Buff(Type: {Type}, value: {Value}, RoundsLeft: {RoundsLeft}, BuffApplicationType: {BuffApplicationType})";
        }

        public Buff Clone()
        {
            return new Buff(Type, Value, RoundsLeft, BuffApplicationType);
        }
    }
}
