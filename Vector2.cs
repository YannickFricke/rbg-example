namespace Rbg
{
    public class Vector2
    {
        public int X;

        public int Y;

        public Vector2(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Vector2 Zero = new(0, 0);
    }
}
