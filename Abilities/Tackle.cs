namespace Rbg.Abilities
{
    public class Tackle : CharacterAbility
    {
        public Tackle() : base("Tackle", DamageType.Physical) { }

        public override void Use(PlacedCharacter source, Vector2 position, PlacedCharacter[] afftectedCharacters)
        {
            var damageToDeal = source.Character.PhysicalAttackDamage;

            foreach (var afftectedCharacter in afftectedCharacters)
            {
                var physicalDamageResistance = afftectedCharacter.Character.PhysicalDamageResistance;
                afftectedCharacter.Character.DamageTaken += Utils.Clamp(damageToDeal - physicalDamageResistance, 0, double.MaxValue);

                if (afftectedCharacter.Character.IsAlive)
                {
                    // Character is still alive so we dont need to remove it from the battlefield
                    continue;
                }

                // Character died - remove it from Battlefield
            }
        }
    }
}
